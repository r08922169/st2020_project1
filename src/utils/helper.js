
let sorting = (array) => {
    return array.sort();
}

let compare = (a, b) => {
    let attr = "PM2.5";

    return a[attr] - b[attr];
}

let average = (nums) => {
    let sum = 0;
    let count = 0;
    nums.forEach(num => {
        if (isNaN(num))
            return;

        sum += num;
        count++;
    });

    if (count == 0)
        return 0;

    let average = sum / count;
    average = Math.round(average * 100) / 100;

    return average;
}


module.exports = {
    sorting,
    compare,
    average
}
