const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect;


test('Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
})

test('Should keep default values', () => {
  const values1 = { 'location': 'school' };
  const values2 = { 'name': 'Tony' };
  const values3 = { 'location': 'restaurant', 'name': 'Tony', 'age': 24 };

  const expected = { 'location': 'school', 'name': 'Tony', 'age': 24 }

  let form = _.defaults(values1, values2, values3);
  expect(form).to.eql(expected);
})

test('Should seperate array properly', () => {
  const array = [1, 2, 3, 4, 5, 6, 7];
  const chunkSize = 3;
  const expected = [[1, 2, 3], [4, 5, 6], [7]];

  let result = _.chunk(array, chunkSize);

  expect(result).to.eql(expected);
})

test('Should return third element', () => {
  const array = [1, 2, 3, 4, 5, 6];
  const select = 2;
  const expected = 3;

  let result = _.nth(array, select);

  expect(result).to.eql(expected);
})

test('Should return the last element', () => {
  const array = [1, 2, 3, 4, 5, 6];
  const expected = 6;

  let result = _.last(array);

  expect(result).to.eql(expected);
})

test('Should return the index of the second 3', () => {
  const array = [1, 2, 3, 4, 3, 4];
  const select = 3;
  const expected = 4;

  let result = _.lastIndexOf(array, select);

  expect(result).to.eql(expected);
})

test('Should remove odd numbers', () => {
  const array = [1, 2, 3, 4, 3, 4];
  const filter = n => n % 2 == 1;
  const expected = [2, 4, 4];
  const expected2 = [1, 3, 3];

  let result = _.remove(array, filter);

  expect(array).to.eql(expected);
  expect(result).to.eql(expected2);
})

test('Should keep numbers in the middle', () => {
  const array = [1, 2, 3, 4, 5];
  const start = 1;
  const end = array.length - 1;
  const expected = [2, 3, 4];
  const result = _.slice(array, start, end);

  expect(result).to.eql(expected);
})

test('Should remove the first element', () => {
  const array = [1, 2, 3, 4, 3, 4];
  const expected = [2, 3, 4, 3, 4];

  let result = _.tail(array);

  expect(result).to.eql(expected);
})

test('Should keep first 4 elements', () => {
  const array = [1, 2, 3, 4, 3, 4];
  const size = 4;
  const expected = [1, 2, 3, 4];

  let result = _.take(array, size);

  expect(result).to.eql(expected);
})

test('Should leave no element', () => {
  const array1 = [1, 2, 3];
  const array2 = [3, 2, 1];
  const expected = [];

  let result = _.xor(array1, array2);

  expect(result).to.eql(expected);
})
